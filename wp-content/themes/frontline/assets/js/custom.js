// On document ready functions
// ======================================================================

// If JavaScript is enabled remove 'no-js' class and give 'js' class
jQuery('html').removeClass('no-js').addClass('js');

jQuery(document).ready(function($) {

	// FitVid Magic - Target all videos
	jQuery("body").fitVids();
	 
 	 //Validate forms 
	 if (jQuery().validate) {
	 	jQuery("#contactForm").validate();
	 	jQuery("#commentform").validate();
	 	jQuery("#quickcontact").validate();
	 }
	
	// Tabs and Toggle
	jQuery(".radium-tabs").tabs({ fx: { opacity: 'show' } });
	
	jQuery(".radium-toggle").each( function () {
		if(jQuery(this).attr('data-id') == 'closed') {
			jQuery(this).accordion({ header: '.radium-toggle-title', collapsible: true, active: false  });
		} else {
			jQuery(this).accordion({ header: '.radium-toggle-title', collapsible: true});
		}
	});
		
	// Message box - close buttons
	jQuery(".messageBox .closeBox").click( function() {
		jQuery(this).parent('.messageBox').fadeTo(400, 0.001).slideUp();
	});
    
   	//activate the mega menu
	if(jQuery.fn.radiumMegamenu)		
	jQuery(".main_menu .radium_mega").radiumMegamenu({delay:20});
	
	//Back to Top
	jQuery().UItoTop({ 
		text: 'Top',
		easingType: 'easeOutQuart'
	});
	
	//BacktoTop icon Animation
	jQuery(function() {
	
	    var d=300;
	    
	    jQuery('a#toTop').each(function(){
	        jQuery(this).stop().animate({
	            'marginRight':'-40px'
	        },d+=150);
	    });
	
	    jQuery('a#toTop').hover(
		    function () {
		        jQuery($(this)).stop().animate({
		            'marginRight':'0'
		        },200);
		    },
		    function () {
		        jQuery($(this)).stop().animate({
		            'marginRight':'-40px'
		        },200);
		    }
		);
		
	});
	
	/* Sticky Footer */
	jQuery(window).load(function () {
		jQuery("footer#bottom-footer").stickyFooter();
	});
	
	//Gallery Slider 
	jQuery.fn.slideFadeToggle = function(speed, easing, callback) {
	  return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};

	$('.show_captions').click( function() {
		$(this).parents('.flexslider').find(".flex-caption").each(function (i) {
        if (this.style.display == "none") {
          $(this).show().animate({opacity:1}, 500);
        } else {
          $(this).animate({opacity:0}, 500, function(){$(this).hide();});
        }
      });

		$(this).text($(this).text() == 'Caption' ? 'Hide caption' : 'Caption');
		return false;
	});
	$('.show_thumbnails').click( function() {
		$(this).parents('.flexslider').find('ul.radium_slideshow_thumbnails').slideFadeToggle();
		$(this).text($(this).text() == 'Hide thumbnails' ? 'Show thumbnails' : 'Hide thumbnails');
		return false;
	});	
	
	/* Isotope -------------------------------------*/
	if( jQuery().isotope ) {
	    
	    jQuery(function() {

            var container = jQuery('.isotope'),
                optionFilter = jQuery('#sort-by'),
                optionFilterLinks = optionFilter.find('a');
            
            optionFilterLinks.attr('href', '#');
            
            optionFilterLinks.click(function(){
                var selector = jQuery(this).attr('data-filter');
                container.isotope({ 
                    filter : '.' + selector, 
                    itemSelector : '.page-grid-item',
                    layoutMode : 'fitRows',
                    animationEngine : 'best-available'
                });
                
                // Highlight the correct filter
                optionFilterLinks.removeClass('active');
                jQuery(this).addClass('active');
                return false;
            });
            
	    });
    
	}
	
	/* ---------------------------------------------------------------------- */
	/* Content Carousel 
	/* ---------------------------------------------------------------------- */

	(function() {

		var $carousel = $('.radium-carousel');

		if( $carousel.length ) {

			var scrollCount;

			function getWindowWidth() {

				if( $(window).width() < 480 ) {
					scrollCount = 1;
				} else if( $(window).width() < 768 ) {
					scrollCount = 2;
				} else if( $(window).width() < 960 ) {
					scrollCount = 3;
				} else {
					scrollCount = 4;
				}

			}

			function initCarousel( carousels ) {

				carousels.each(function() {

					var $this  = $(this);

					$this.jcarousel({
						animation : 600,
						easing : 'easeOutCubic',
						scroll : scrollCount,
						itemFirstInCallback : function() {
							onAfterAnimation : resetPosition( $this )
						}
					});

				});

			}

			function adjustCarousel() {

				$carousel.each(function() {

					var $this    = $(this),
						$lis     = $this.children('li')
						newWidth = $lis.length * $lis.first().outerWidth( true ) + 100;

					getWindowWidth();

					// Resize only if width has changed
					if( $this.width() !== newWidth ) {

						$this.css('width', newWidth )
							 .data('resize','true');

						initCarousel( $this );

						$this.jcarousel('scroll', 1);

						var timer = window.setTimeout( function() {
							window.clearTimeout( timer );
							$this.data('resize', null);
						}, 600 );

					}

				});

			}

			function resetPosition( elem, resizeEvent ) {
				if( elem.data('resize') )
					elem.css('left', '0');
			}

			getWindowWidth();

			initCarousel( $carousel );

			// Window resize
			$(window).on('resize', function() {

				var timer = window.setTimeout( function() {
					window.clearTimeout( timer );
					adjustCarousel();
				}, 30 );

			});
		}

	})();

	/* end Post Carousel */




});

// make code pretty
window.prettyPrint && prettyPrint()

 
/*-----------------------------------------------------------------------------------*/
//	Preload
/*-----------------------------------------------------------------------------------*/
jQuery(window).bind('load', function() {
     var i = 1;
     var imgs = jQuery('.post-thumb.preload img').length;
     var int = setInterval(function() {

     if(i >= imgs) clearInterval(int);
     jQuery('.post-thumb img:not(.image-loaded)').eq(0).animate({ opacity: "1"}, 300,"easeInQuart").addClass('image-loaded');
     i++;
     
     }, 100);   
     
    jQuery('div').removeClass('preload');  
    
    jQuery('.post-thumb a img').hover(
        function () {
            jQuery(jQuery(this)).stop().animate({
                opacity: "0.3"
            },300);
        },
        function () {
            jQuery(jQuery(this)).stop().animate({
                opacity: "1"
            },300);
        }
    );

});


/* ----------------------------------------------------- */
/* Radium Sticky Footer Plugin */
/* ----------------------------------------------------- */

  (function($){
  
    var footer;
  
    $.fn.extend({
    
      stickyFooter: function(options) {
        footer = this;
        
        positionFooter();
  
        $(window).scroll(positionFooter).resize(positionFooter);
  
        function positionFooter() {
          var docHeight = $(document.body).height() - $("#footer-push").height();
          if(docHeight < $(window).height()){
            var diff = $(window).height() - docHeight;
            if (!$("#footer-push").length > 0) {
              $(footer).before('<div id="footer-push"></div>');
            }
            $("#sticky-footer-push").height(diff);
          }
        }
      }
    });
    
  })(jQuery);
   

/* ----------------------------------------------------- */
/* Radium Mega Menu*/
/* ----------------------------------------------------- */

(function(a){a.fn.radiumMegamenu=function(b){var c={modify_position:true,delay:300};var d=a.extend(c,b);return this.each(function(){function i(a){if(h[a]==true){var b=e.filter(":eq("+a+")").css({overflow:"visible"}).find("div:first"),c=e.filter(":eq("+a+")").find("a:first");b.stop().css("display","block").animate({opacity:1},200);if(b.length){c.addClass("open-mega-a")}}}function j(b){if(h[b]==false){e.filter(":eq("+b+")").find(">a").removeClass("open-mega-a");var c=e.filter(":eq("+b+")"),d=c.find("div:first");d.stop().css("display","block").animate({opacity:0},300,function(){a(this).css("display","none");c.css({overflow:"hidden"})})}}var b=a(this),c=b.find(">li"),e=c.find(">div").parent().css({overflow:"hidden"}),f=c.find(">ul").parent(),g=b.parent().width(),h={};c.each(function(){var b=a(this),c=b.position(),e=b.find("div:first").css({opacity:0,display:"none"}),f="";if(!e.length){f=b.find(">ul").css({display:"none"})}if(e.length||f.length){var h=b.addClass("dropdown_ul_available").find(">a");h.html("<span class='dropdown_link'>"+h.html()+"</span>").append('<span class="dropdown_available"></span>');if(typeof h.attr("href")!="string"){h.css("cursor","default")}}if(d.modify_position&&e.length){if(e.width()>c.left){e.css({left:Math.ceil()*-1})}else if(c.left+e.width()>g){e.css({left:(e.width()-c.left)*-1})}}});e.each(function(b){a(this).hover(function(){h[b]=true;setTimeout(function(){i(b)},d.delay)},function(){h[b]=false;setTimeout(function(){j(b)},d.delay)})});f.find("li").andSelf().each(function(){var b=a(this),c=b.find("ul:first"),d=false;if(c.length){c.css({display:"block",opacity:0,visibility:"hidden"});var e=b.find(">a");e.bind("mouseenter",function(){c.stop().css({visibility:"visible"}).animate({opacity:1})});b.bind("mouseleave",function(){c.stop().animate({opacity:0},function(){c.css({visibility:"hidden"})})})}})})}})(jQuery);

 /*
  * jQuery Custom Forms Plugin 1.0
  * www.ZURB.com
  * Copyright 2010, ZURB
  * Free to use under the MIT license.
  * http://www.opensource.org/licenses/mit-license.php
 */
 
 jQuery.foundation = jQuery.foundation || {};
 jQuery.foundation.customForms = jQuery.foundation.customForms || {};
 
 jQuery(document).ready(function ($) {
   
   function appendCustomMarkup(type) {
     $('form.custom input:' + type).each(function () {
 
       var $this = $(this).hide(),
           $span = $this.next('span.custom.' + type);
 
       if ($span.length === 0) {
         $span = $('<span class="custom ' + type + '"></span>').insertAfter($this);
       }
 
       $span.toggleClass('checked', $this.is(':checked'));
       $span.toggleClass('disabled', $this.is(':disabled'));
     });
   }
 
   function appendCustomSelect(sel) {
     var $this = $(sel),
         $customSelect = $this.next('div.custom.dropdown'),
         $options = $this.find('option'),
         maxWidth = 0,
         $li;
 
     if ($this.hasClass('no-custom')) { return; }
     if ($customSelect.length === 0) {
       $customSelectSize = '';
       if ($(sel).hasClass('small')) {
       	$customSelectSize = 'small';
       } else if ($(sel).hasClass('medium')) {
       	$customSelectSize = 'medium';
       } else if ($(sel).hasClass('large')) {
       	$customSelectSize = 'large';
       } else if ($(sel).hasClass('expand')) {
       	$customSelectSize = 'expand';
       }
       $customSelect = $('<div class="custom dropdown ' + $customSelectSize + '"><a href="#" class="selector"></a><ul></ul></div>"');
       $options.each(function () {
         $li = $('<li>' + $(this).html() + '</li>');
         $customSelect.find('ul').append($li);
       });
       $customSelect.prepend('<a href="#" class="current">' + $options.first().html() + '</a>');
 
       $this.after($customSelect);
       $this.hide();
       
     } else {
       // refresh the ul with options from the select in case the supplied markup doesn't match
       $customSelect.find('ul').html('');
       $options.each(function () {
         $li = $('<li>' + $(this).html() + '</li>');
         $customSelect.find('ul').append($li);
       });
     }
 
     $customSelect.toggleClass('disabled', $this.is(':disabled'));
 
     $options.each(function (index) {
       if (this.selected) {
         $customSelect.find('li').eq(index).addClass('selected');
         $customSelect.find('.current').html($(this).html());
       }
     });
 
     $customSelect.css('width', 'inherit');
     $customSelect.find('ul').css('width', 'inherit');
 
     $customSelect.find('li').each(function () {
       $customSelect.addClass('open');
       if ($(this).outerWidth() > maxWidth) {
         maxWidth = $(this).outerWidth();
       }
       $customSelect.removeClass('open');
     });
     
     if (!$customSelect.is('.small, .medium, .large, .expand')) {
       $customSelect.css('width', maxWidth + 18 + 'px');
       $customSelect.find('ul').css('width', maxWidth + 16 + 'px');
     }
 
   }
   
   $.foundation.customForms.appendCustomMarkup = function () {
     appendCustomMarkup('checkbox');
     appendCustomMarkup('radio');
   
     $('form.custom select').each(function () {
       appendCustomSelect(this);
     });
   };
 
   $.foundation.customForms.appendCustomMarkup();
 });
 
 (function ($) {
   
   function refreshCustomSelect($select) {
     var maxWidth = 0,
         $customSelect = $select.next();
     $options = $select.find('option');
     $customSelect.find('ul').html('');
     
     $options.each(function () {
       $li = $('<li>' + $(this).html() + '</li>');
       $customSelect.find('ul').append($li);
     });
     
     // re-populate
     $options.each(function (index) {
       if (this.selected) {
         $customSelect.find('li').eq(index).addClass('selected');
         $customSelect.find('.current').html($(this).html());
       }
     });
     
     // fix width
     $customSelect.removeAttr('style')
       .find('ul').removeAttr('style');
     $customSelect.find('li').each(function () {
       $customSelect.addClass('open');
       if ($(this).outerWidth() > maxWidth) {
         maxWidth = $(this).outerWidth();
       }
       $customSelect.removeClass('open');
     });
     $customSelect.css('width', maxWidth + 18 + 'px');
     $customSelect.find('ul').css('width', maxWidth + 16 + 'px');
     
   }
   
   function toggleCheckbox($element) {
     var $input = $element.prev(),
         input = $input[0];
 
     if (false == $input.is(':disabled')) {
         input.checked = ((input.checked) ? false : true);
         $element.toggleClass('checked');
 
         $input.trigger('change');
     }
   }
   
   function toggleRadio($element) {
     var $input = $element.prev(),
         input = $input[0];
 
     if (false == $input.is(':disabled')) {
       $('input:radio[name="' + $input.attr('name') + '"]').each(function () {
         $(this).next().removeClass('checked');
       });
       input.checked = ((input.checked) ? false : true);
       $element.toggleClass('checked');
     
       $input.trigger('change');
     }
   }
   
   $('form.custom span.custom.checkbox').live('click', function (event) {
     event.preventDefault();
     event.stopPropagation();
     
     toggleCheckbox($(this));
   });
   
   $('form.custom span.custom.radio').live('click', function (event) {
     event.preventDefault();
     event.stopPropagation();
     
     toggleRadio($(this));
   });
   
   $('form.custom select').live('change', function (event) {
     refreshCustomSelect($(this));
   });
   
   $('form.custom label').live('click', function (event) {
     var $associatedElement = $('#' + $(this).attr('for')),
         $customCheckbox,
         $customRadio;
     if ($associatedElement.length !== 0) {
       if ($associatedElement.attr('type') === 'checkbox') {
         event.preventDefault();
         $customCheckbox = $(this).find('span.custom.checkbox');
         toggleCheckbox($customCheckbox);
       } else if ($associatedElement.attr('type') === 'radio') {
         event.preventDefault();
         $customRadio = $(this).find('span.custom.radio');
         toggleRadio($customRadio);
       }
     }
   });
 
   $('form.custom div.custom.dropdown a.current, form.custom div.custom.dropdown a.selector').live('click', function (event) {
     var $this = $(this),
         $dropdown = $this.closest('div.custom.dropdown'),
         $select = $dropdown.prev();
     
     event.preventDefault();
 
     if (false == $select.is(':disabled')) {
         $dropdown.toggleClass('open');
 
         if ($dropdown.hasClass('open')) {
           $(document).bind('click.customdropdown', function (event) {
             $dropdown.removeClass('open');
             $(document).unbind('.customdropdown');
           });
         } else {
           $(document).unbind('.customdropdown');
         }
         return false;
     }
   });
   
   $('form.custom div.custom.dropdown li').live('click', function (event) {
     var $this = $(this),
         $customDropdown = $this.closest('div.custom.dropdown'),
         $select = $customDropdown.prev(),
         selectedIndex = 0;
         
     event.preventDefault();
     event.stopPropagation();
     
     $this
       .closest('ul')
       .find('li')
       .removeClass('selected');
     $this.addClass('selected');
     
     $customDropdown
       .removeClass('open')
       .find('a.current')
       .html($this.html());
     
     $this.closest('ul').find('li').each(function (index) {
       if ($this[0] == this) {
         selectedIndex = index;
       }
       
     });
     $select[0].selectedIndex = selectedIndex;
     
     $select.trigger('change');
   });
 })(jQuery);


/*-----------------------------------------------------------------------------------*/
/*	Magic Search
/*-----------------------------------------------------------------------------------*/	
function RadiumGetUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];    
    }
    return vars;
}

var searchstring = RadiumGetUrlVars()['s'];

if (searchstring) {
	 var searchstring = searchstring.split('+').join(' ');
	 jQuery('#top input.s').val(searchstring).animate({width:220, backgroundColor:'#fff'},450,'easeInOutExpo');
} else {
	jQuery("#top input.s").val('Search for something...');
	jQuery('#top input.s').focus(function() {
				jQuery(this).val('').animate({width:220, backgroundColor:'#fff'},450,'easeInOutExpo');
			});
			jQuery('#top input.s').blur(function() {
				jQuery(this).val('Search for something...').animate({width:180, backgroundColor:'#e1e1e1'},450,'easeInOutExpo');
	});
}
